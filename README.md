# README

This is an experimental rewrite of the Features module for Drupal 8. The
infrastructure of configuration management in Drupal core has replaced a lot of
heavy lifting, but Features will live on as a helper mechanism to create
individually maintained, swappable, and reusable segments of site functionality.

This README will self-destruct once it is no longer needed to coordinate
development.
